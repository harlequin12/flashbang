package com.android.flashbang01;



import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.MotionEventCompat;
import android.text.Html;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;


/**
 * This is the class that acts on behalf of the main activity.
 * This activity handles listing the book marks and launching them.
 *
 */


public class FlashActivity extends ListActivity {
//public class FlashActivity extends Activity {
	
   
	// Views n Buttons
	private TextView mSerialText;
	private Button mButWpmPlus;
	private Button mButWpmMinus;
	private Button mButLeftWord;
	private Button mButLeftPeriod;
	private Button mButRightWord;
	private Button mButRightPeriod;
	private TextView mWpmText;
	
	
	// Db Adapter
	private DbAdapter mDbHelper;
	
	// Book Variables
	private Book mBook;
    private Boolean go;
    private AccurateTimer flashTimer;
    private BookLineAdapter bookLineAdapter;
	
    // Length Distribution
    private double[] mNormLenDistro;
    private double mMeanLen;
	private double mStdDev;
    private double mPunctProb;
	
    // Revert data - for if user chooses to discard
    private int revertReadTime;
    private int revertWpm;
    private int revertWordPos;
    private int revertLinePos;
    
    // Settings
    private Boolean mDynamicFontSize;
    ArrayAdapter<String> adapter;
    
    
    /** 
     * Called when the activity (main page - the list of book marks) is first created. 
     * */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flash_activity_portrait_serial_para);
        
        mDbHelper = new DbAdapter(this);
        mDbHelper.open();
        
      	Bundle extras = getIntent().getExtras();
		long rowId = extras.getLong(DbAdapter.KEY_ROWID);
        mBook = mDbHelper.fetchBook(rowId);	
            
        setupView();         
        //in para view move to where the current word is
        setupDistro();
        
        // Setting reverting data
        revertReadTime = mBook.getRead_time();
        revertWpm = mBook.getWpm();
        revertWordPos = mBook.getWordPos();
        revertLinePos = mBook.getLinePos();


        go = false;
        flashTimer = null;
        if(mBook != null){
        	mSerialText.setText(mBook.getWord()); 
        	
        	bookLineAdapter = new BookLineAdapter(
        			this, R.layout.flash_para_row, R.id.para_row_text, mBook);        	
            setListAdapter(bookLineAdapter);
        }else
        	mSerialText.setText("Failed to open book."); 	
        
    }

    /**
     * onCreate Helper Functions
     */
    
    private class BookLineAdapter extends ArrayAdapter<String>{
    	Book book;

    	public BookLineAdapter(Context context, int resource, int textViewResourceId, Book b) {
    		super(context, resource, textViewResourceId, b.getLineList());
    		book = b;
    	}


    	@Override
    	public View getView(final int pos, View convertView, final ViewGroup parent){
    		TextView mView = (TextView)convertView;
    		if(null == mView){ 
    			mView = new TextView(parent.getContext());
    			mView.setTextSize(12);
    		}

    		if( pos == book.getLinePos() ){
    			//Highlight the word here.
    			String lineArray[] = getItem(pos).split("[ \t\n\r]");
    			StringBuilder line = new StringBuilder("");
    			
    			//line.append("<br>"); <-- Adding this new line and the corresponding one below 
    			// 							can be toggled with a preference later.
    			
    			// I dont like this because it messes up the white space of
    			// the line which doesn't look good on the line view. 
    			// Need another way. Use regex perhaps?
    			
    			for( int i = 0; i < lineArray.length; i++){
    				if(i == book.getWordPos() ){
    					line.append("<font color='red'>");
    					line.append(lineArray[i]);
    					line.append("</font> ");
    				}else{
    					line.append(lineArray[i]);
    					line.append(" ");
    				}
    			}
    			//line.append("<br>");
				
    			mView.setText(Html.fromHtml(line.toString()));
    		}else
    			mView.setText(getItem(pos));


    		return mView;
    	}
    }
    
    

 
    private void setupDistro(){
	    // Statistics
	    double[] lenProb = new double[21];
	    int wordCount = mBook.getLengthDistro()[0];
	    if( wordCount <= 0) // No dividing by zero
	    	wordCount = 1;
	    
	    // Finding the mean
	    mMeanLen = 0;
	    for( int i = 1; i <= 20; i++){
	    	lenProb[i] = (double)mBook.getLengthDistro()[i]/(double)wordCount;
	    	mMeanLen += lenProb[i]*i;
	    }
	    mPunctProb = (double)mBook.getLengthDistro()[20]/(double)wordCount;
	    
		        
	    mStdDev = 0;
	    for( int i = 1; i <= 20; i++)
	        mStdDev += ((Math.pow((i-mMeanLen),2)) * lenProb[i]);
	    
	    //mStdDev = mStdDev/(double)20;
	    mStdDev = Math.sqrt(mStdDev);
		
	    mNormLenDistro = new double[21];
	    for( int i = 0; i <= 20; i++)
	    	mNormLenDistro[i] = (i - mMeanLen)/mStdDev;	    
	}
    

	private void setupView(){
    	
        
		// Initialize the default settings if it has never been done before.
		PreferenceManager.setDefaultValues(this, R.layout.preferences, false);
    	SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
       
    	// Setting up the views according to the orientation of the screen
        int orient = getResources().getConfiguration().orientation;

    	
//    	if( sharedPref.getBoolean("ignoreOrient", true) ){
//    		if( sharedPref.getBoolean("lockPos", true) ){	//lanscape
//    			//check for serial vew
//    	        setContentView(R.layout.flash_activity_portrait_serial_para);
//    	
//    		}else{	//Portrait
//    			//check for serial vew
//    	        setContentView(R.layout.flash_activity_portrait_serial_para);
//
//    		}
//    	
//    	}else 
    	if( orient == Configuration.ORIENTATION_PORTRAIT ){
        	if( sharedPref.getBoolean("pref_para_on_portrait", true) ){
        		
        		//show the para view
                setContentView(R.layout.flash_activity_portrait_serial_para);
        	}else{
        		//hide the para view
                setContentView(R.layout.flash_activity_serial);
        		
        	}
    	
    	}else if( orient == Configuration.ORIENTATION_LANDSCAPE ){
    		if( sharedPref.getBoolean("pref_para_on_landscape", false) ){
        		
        		//show the serial view
    	        setContentView(R.layout.flash_activity_portrait_serial_para);
        	}else{
        		//hide the serial view
                setContentView(R.layout.flash_activity_serial);
        	}
    	}

        mSerialText = (TextView) findViewById(R.id.serial_field);
        
        
        // Set the font size if 
    	mDynamicFontSize = sharedPref.getBoolean("enable_dynamic_font", true);
    	if(mDynamicFontSize == false){
    		String fontChoice = sharedPref.getString("serial_font_size", "1");
    		if(fontChoice.contentEquals("0"))
    				mSerialText.setTextSize(TypedValue.COMPLEX_UNIT_SP,22);
    		if(fontChoice.contentEquals("1"))
        			mSerialText.setTextSize(TypedValue.COMPLEX_UNIT_SP,32);
    		if(fontChoice.contentEquals("2"))
        			mSerialText.setTextSize(TypedValue.COMPLEX_UNIT_SP,42);
    		if(fontChoice.contentEquals("3"))
        			mSerialText.setTextSize(TypedValue.COMPLEX_UNIT_SP,55);
    	}
    	
        
        // Get the buttons
    	mButWpmPlus = (Button) findViewById(R.id.wpm_plus);
    	mButWpmMinus = (Button) findViewById(R.id.wpm_minus);
    	mWpmText = (TextView) findViewById(R.id.wpm_text);

    	mButLeftWord = (Button) findViewById(R.id.nav_left_word);
    	mButLeftPeriod = (Button) findViewById(R.id.nav_left_period);
    	mButRightWord = (Button) findViewById(R.id.nav_right_word);
    	mButRightPeriod = (Button) findViewById(R.id.nav_right_period);
    	
    	mButWpmPlus.setVisibility(View.VISIBLE);
    	mButWpmMinus.setVisibility(View.VISIBLE);
    	mWpmText.setVisibility(View.VISIBLE);
    	mWpmText.setText(String.valueOf(mBook.getWpm()));
    	
    	mButLeftWord.setVisibility(View.VISIBLE);
    	mButLeftPeriod.setVisibility(View.VISIBLE);
    	mButRightWord.setVisibility(View.VISIBLE);
    	mButRightPeriod.setVisibility(View.VISIBLE);
	}        

   

	private void flash(long timeMillis){
		flashTimer = new AccurateTimer(timeMillis) {
			public void onFinish() {
				mBook.incWord();
				StringBuilder nextWord = new StringBuilder(mBook.getWord());
				
				// I don't like all of this.
				// I think I want to consider including all punctuation in the word length count
				// because it will add some time to the display time of the word.
				// We will simply check to see if the word ends with any punctuation and add a bit of time
				// with out regard to how much puncuation there is.
				
				int punct = 0;
				int endex = nextWord.length();	//index and end. Bahahaha. Get it? Funny right?
				while( ".,!?:;".contains(nextWord.substring(0,endex)) && endex < 0){
						punct++;
						endex--;
					}
				int wordLen = endex;
			
				
				if(wordLen < 0)
					wordLen = 0;
				if(wordLen > 20)
					wordLen = 20;
				
				if(mDynamicFontSize)
	    			mSerialText.setTextSize(TypedValue.COMPLEX_UNIT_SP,((21 - wordLen) + 23));


				mSerialText.setText(nextWord);
				//if( mParaList != null){
					//What happens when the user does not want to have para view.
				//}
				bookLineAdapter.notifyDataSetChanged();
				setSelection(mBook.getLinePos()-2);
				
				if(go)
					flash((long)(
							(60000/(double)mBook.getWpm()) * 
							(1 + mNormLenDistro[wordLen] + (mPunctProb*punct))) 			);
				
				// Am I subtracting the time needed to make up for punctuation pauses?
				// No I am not. We need to.
				
		    	 // Will this blow the stack????
				// Seriously this is a big concern...
		     }
		  }.start();
	}	


	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		
		mBook.setLinePos(position);
		mSerialText.setText(mBook.getWord());
		bookLineAdapter.notifyDataSetChanged();
		
		// Option to set off flash again on select.
		// 
		// Option to move to sentence that begins on selected line.
	}
	
	
	/**
	 * Button Methods
	 */

	//Navigation buttons
	public void navLeftWord(View item){
		//mBook.decWord();
		//mSerialText.setText(mBook.getWord());
		
		//bookLineAdapter.notifyDataSetChanged();
		//setSelection(mBook.getLinePosition()-2);
	}
	public void navRightWord(View item){
		mBook.incWord();
		mSerialText.setText(mBook.getWord());		
		
		bookLineAdapter.notifyDataSetChanged();
		setSelection(mBook.getLinePos()-2);
	}
	
	public void navLeftPeriod(View item){
		
	}
	public void navRightPeriod(View item){
		
	}
	
	//Wpm button handlers
	public void wpmUp(View v){
    	mBook.setWpm(mBook.getWpm() + 20);
    	if(mBook.getWpm() > 600)
    		mBook.setWpm(600);
    	mWpmText.setText(String.valueOf(mBook.getWpm()));
	}
	public void wpmDown(View v){
		mBook.setWpm(mBook.getWpm() - 20);
    	if( mBook.getWpm() < 20) 
    		mBook.setWpm(20);
    	mWpmText.setText(String.valueOf(mBook.getWpm()));
	}

	
	/**
	 * Button press handlers
	 */
	@Override
    public boolean onTouchEvent(MotionEvent event){
    	  int action = MotionEventCompat.getActionMasked(event);
          
    	    switch(action) {
    	        case (MotionEvent.ACTION_DOWN) :
    		        if(go == true){
    		        	mButWpmPlus.setVisibility(View.VISIBLE);
    		        	mButWpmMinus.setVisibility(View.VISIBLE);
    		        	mWpmText.setVisibility(View.VISIBLE);
    	
    		        	mButLeftWord.setVisibility(View.VISIBLE);
    		        	mButLeftPeriod.setVisibility(View.VISIBLE);
    		        	mButRightWord.setVisibility(View.VISIBLE);
    		        	mButRightPeriod.setVisibility(View.VISIBLE);

    		        	if( flashTimer != null)
    		        		flashTimer.cancel();
    		        	go = false;
    		        }else{
    		        	mButWpmPlus.setVisibility(View.GONE);
    		        	mButWpmMinus.setVisibility(View.GONE);
    		        	mWpmText.setVisibility(View.GONE) ;

    		        	mButLeftWord.setVisibility(View.GONE);
    		        	mButLeftPeriod.setVisibility(View.GONE);
    		        	mButRightWord.setVisibility(View.GONE);
    		        	mButRightPeriod.setVisibility(View.GONE);

    		        	go = true;
    		        	flash(500);
    		        }
    	            return true;
    	        //case (MotionEvent.ACTION_MOVE) :
    	        //    return true;
    	        //case (MotionEvent.ACTION_UP) :
    	        //    return true;
    	        //case (MotionEvent.ACTION_CANCEL) :
    	        //    return true;
    	        //case (MotionEvent.ACTION_OUTSIDE) :
    	        //    return true;      
    	        default : 
    	            return super.onTouchEvent(event);
    	    }   
    }
	
	/*
	 * Overriding what happens when you press back or menu buttons
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if(keyCode == KeyEvent.KEYCODE_BACK ||
		   keyCode == KeyEvent.KEYCODE_MENU){
	    	
			// Stop the flashing.
	        if( flashTimer != null)
	    		flashTimer.cancel();
	    	go = false;
			
	    	// Build and show the dialog.
		    AlertDialog.Builder builder = new AlertDialog.Builder(this);
		    //builder.setTitle(R.string.pick_color);
		     String[] options = {
		            "Save Progress",
		            "Discard Progress",
		            "Settings"
		        };
		    builder.setItems(options, new DialogInterface.OnClickListener() {
		               public void onClick(DialogInterface dialog, int which) {
		               // The 'which' argument contains the index position
		               // of the selected item
		            	   
		            	   switch(which){
		            	   case 0:	//Save Progress
		            		   finish();
		            		   mDbHelper.updateBook(mBook);
		            		   break;
		            		   
		            	   case 1:
		            		   // Revert book state to initial state
		            	       mBook.setRead_time( revertReadTime );
		            	       mBook.setWpm( revertWpm );
		            	       mBook.setWordPos( revertWordPos );
		            	       mBook.setLinePos( revertLinePos );
		            		   finish(); 
		            		   // Keep this after because it will do the saving asynchronously.
		            		   // Keeps things snappy.
		            		   mDbHelper.updateBook(mBook);
		            		   break;
		            		   
		            	   case 2:
		            		   Intent i = new Intent(getBaseContext(), DefaultPreferences.class);
		            		   startActivity(i);
		            		   break;
		            	   }
		           }
		    });
		    AlertDialog dialog = builder.create();
			dialog.show();
		}
			
		return super.onKeyDown(keyCode, event);
	}
	
	
	
    
    /**
     * State maintenance functions
     */
	@Override
	protected void onResume(){
	    super.onResume();
	    setupView();
	}    
	
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        
        // Doing this here creates an opportunity for the user to accidentally
        // save reading progress when it may be undesired.
        mDbHelper.updateBook(mBook);
        
        
        //outState.putSerializable(DbAdapter.KEY_ROWID, mBook.getRowId());
    }
    
    @Override
    protected void onPause() {
        super.onPause();

        // Pause when the user leaves this activity
        if( flashTimer != null)
    		flashTimer.cancel();
    	go = false;

    	// Possible undesired saving of reading progress.
    	mDbHelper.updateBook(mBook);
    	//Toast.makeText(this, "1 charPos: " + mBook.getCharPosition() + " " + String.valueOf(ret), Toast.LENGTH_SHORT).show();
    }
   
    
}
