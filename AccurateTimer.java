package com.android.flashbang01;

import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;


public abstract class AccurateTimer {

    private final long mMillisUntilOver;
    private long mStopTimeInFuture;
    private static final int MSG = 1;


    public AccurateTimer(long millisUntilOver){ 
        mMillisUntilOver= millisUntilOver;
    }

    public final void cancel() {
        mHandler.removeMessages(MSG);
    }

    //  Callback fired when the time is up.
    public abstract void onFinish();

 
    //  Start the countdown.
    public synchronized final AccurateTimer start() {
        if (mMillisUntilOver <= 0) {
            onFinish();
            return this;
        }
        //mStopTimeInFuture = SystemClock.elapsedRealtime() + mMillisInFuture;
        mStopTimeInFuture = SystemClock.uptimeMillis() + mMillisUntilOver;

        mHandler.sendMessageAtTime(mHandler.obtainMessage(MSG), mStopTimeInFuture);
        return this;
    }
    

    // handles counting down
   private Handler mHandler = new Handler() {

    	@Override
    	public void handleMessage(Message msg) {
    	    synchronized (AccurateTimer.this) {
    	        final long millisLeft = mStopTimeInFuture - SystemClock.uptimeMillis();

    	        if (millisLeft <= 0) {
    	            onFinish();
    	        } else {
    	        	sendMessageAtTime(obtainMessage(MSG), mStopTimeInFuture);
    	        }
    	    }
    	}
   };
}