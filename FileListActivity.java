package com.android.flashbang01;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class FileListActivity extends ListActivity{
	
	// All this is derived from this tutorial:
	// http://www.dreamincode.net/forums/topic/190013-creating-simple-file-chooser/

	// To do:
	// -Change the way the user goes back to a parent directory from current position.
	//  This should be done with the action bar, perhaps by clicking the app icon.
	private File currentDir;
	private File initDir;
	private FileArrayAdapter adapter;
	//private String bookmarkTitle;
	private DbAdapter mDbHelper;
	
	
	public class FileArrayAdapter extends ArrayAdapter<File>{

		private Context c;
		private int id;
		private List<File>items;
		
		public FileArrayAdapter(Context context, int textViewResourceId, List<File> objects){
			super(context, textViewResourceId, objects);
			c = context;
			id = textViewResourceId;
			items = objects;
		}
		
		
		@Override
	    public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
	        if (v == null) {
	        	LayoutInflater vi = (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	        	v = vi.inflate(id, null);
	        }
	        final File o = items.get(position);
	        if (o != null) {
	        	TextView t1 = (TextView) v.findViewById(R.id.TextView01);
	        	TextView t2 = (TextView) v.findViewById(R.id.TextView02);
	                       
	        	if(t1!=null)
	        		t1.setText(o.getName());
	        	if(t2!=null)
	        		t2.setText(o.getPath());
	                       
	        }
	        return v;
		}
	}

    /** 
     * Called when the activity (main page - the list of book marks) is first created. 
     * */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fileselect_list);
        
    	mDbHelper = new DbAdapter(this);
    	mDbHelper.open();
        
        // Get the default start directory
        initDir = Environment.getExternalStorageDirectory();
        currentDir = initDir;
        //bookmarkTitle = initDir.getName();
        // I want this to be based on intent data later

        //ActionBar bar = getActionBar();
        //bar.setHomeButtonEnabled(true);
        
        fillList(currentDir);
        //registerForContextMenu(getListView());	
        
    }
    
    private Boolean checkFileType(File dir){    	
    	// This does a recursive depth first search to see if dir is either a file with
    	// an acceptable type or if it is a directory that contains a file with
    	// an acceptable type.
    	
    	File[] filesInDir = dir.listFiles();
    
    	if(filesInDir != null){	// It is a driectory
    		for(File each : filesInDir){
    			if(checkFileType(each))
    				return true;
			}
    	}else{ //Is actually a file, not a directory at this point.
    		String name = dir.getName().toLowerCase();

            for (String extension: getResources().getStringArray(R.array.acceptable_file_types)){
            	if (name.endsWith(("." + extension).toLowerCase()))
                    return true;
            }
    	}
    	return false;
    }
    
	private void fillList(File f)
        {
			File[]directories= f.listFiles();
			this.setTitle(f.getPath());
		
			List<File>dir = new ArrayList<File>();
			List<File>fls = new ArrayList<File>();
			try{
				for(File element: directories){	
					if(!element.isHidden())
					if(checkFileType(element)){
						dir.add(element.getAbsoluteFile());
					}
                }
            }catch(Exception e){  }
            
			Collections.sort(dir);
            Collections.sort(fls);
			dir.addAll(fls);
            
			if(f.compareTo(initDir) != 0)
                dir.add(0, f.getParentFile());
            adapter = new FileArrayAdapter(
            		FileListActivity.this,R.layout.fileselect_list,dir);
            this.setListAdapter(adapter);
       }

    
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) 
//	{    
//	   switch (item.getItemId()) 
//	   {        
//	      case android.R.id.home:             
//	    	  currentDir = currentDir.getParentFile();
//			  fillList(currentDir);
//			  return true;        
//	      default:            
//	          return super.onOptionsItemSelected(item);    
//	   }
//	}
	
    @Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
	
		File f = adapter.getItem(position).getAbsoluteFile();
		if(f.isDirectory()||f.getName().equalsIgnoreCase("..")){
			currentDir = f.getAbsoluteFile();
			fillList(currentDir);
		}
		else{
			currentDir = f.getAbsoluteFile();
			onFileClick(f);
		}
	}
    
    private void onFileClick(File file)
    {
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setTitle("Name bookmark");

    	// Set up the input
    	final EditText nameInput = new EditText(this);
    	
    	// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
    	nameInput.setInputType(InputType.TYPE_CLASS_TEXT );
    	nameInput.setText(file.getName());
    	builder.setView(nameInput);

    	// Set up the buttons
    	builder.setPositiveButton("OK", new DialogInterface.OnClickListener() { 
    	    @Override
    	    public void onClick(DialogInterface dialog, int which) {
    	    	//bookmarkTitle = nameInput.getText().toString();
    	    	
    	    	Intent resultData = new Intent();
    	    	resultData.putExtra(DbAdapter.KEY_SOURCE, currentDir.getAbsolutePath());
    	    	resultData.putExtra(DbAdapter.KEY_TITLE, nameInput.getText().toString());
    	    	
    	    	setResult(RESULT_OK, resultData);
    	    	finish();
    	    	// Is it right to perform the finish from within the dialog?
    	    }
    	});
    	builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
    	    @Override
    	    public void onClick(DialogInterface dialog, int which) {
    	        dialog.cancel();
    	    }
    	});

    	builder.show();
    	
    
    	
    }
       
    @Override
    protected void onPause() {
        super.onPause();
        setResult(RESULT_CANCELED);
    	finish();
    }
 
}
