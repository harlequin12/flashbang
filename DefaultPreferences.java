package com.android.flashbang01;

import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;


public class DefaultPreferences extends PreferenceActivity {

	private CheckBoxPreference serialDynamicFont;
	private ListPreference serialFontsizeList;

	//private final String PREFERENCES_KEY = "preference_key";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {    
	    super.onCreate(savedInstanceState);       
	    
	    // get the custom preferences name from the extra data in the intent
	    //String preferencesName = getIntent().getExtras().getString(PREFERENCES_KEY);
	    // set the preferences file name
	    //getPreferenceManager().setSharedPreferencesName(preferencesName);
	    // get the default preferences from XML
	    //addPreferencesFromResource(R.xml.preferences);
	    addPreferencesFromResource(R.layout.preferences);  
	    
	    // Finding the Preference Objects
	    serialFontsizeList = (ListPreference)findPreference("serial_font_size");
	    serialDynamicFont = (CheckBoxPreference)findPreference("enable_dynamic_font");
	    
	    // This listener is responsible for the dependency between the 
	    // dynamic font check box and the font size selector.
	    serialDynamicFont.setOnPreferenceChangeListener(
	    		new Preference.OnPreferenceChangeListener() {
	    			public boolean onPreferenceChange(Preference preference, Object newValue) {
	    				//final String val = newValue.toString();
	    				if( !serialDynamicFont.isChecked())	
	    					serialFontsizeList.setEnabled(false);
	    				else
	    					serialFontsizeList.setEnabled(true);
	    				return true;
	    			}
	    		});
	}

}