package com.android.flashbang01;

//import java.util.Date;
import com.android.flashbang01.R;
//import com.android.flashbang01.R.id;
//import com.android.flashbang01.R.layout;
import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class BookmarkInfoActivity extends Activity{

	private DbAdapter mDbHelper;

    private TextView mInfoText;
    //private Button doneButton;
    
    private Long mRowId;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        mDbHelper = new DbAdapter(this);
        mDbHelper.open();
        
        setContentView(R.layout.bookmark_info);        	
        setTitle("Bookmark Info");

        mInfoText = (TextView) findViewById(R.id.info_box);
        //Button doneButton = (Button) findViewById(R.id.infoConfirm);
        
        mRowId = getIntent().getExtras().getLong(DbAdapter.KEY_ROWID);
    
        //populateFields();
        
        populateFields();
    }
    
    public void infoDone(View item){
    	setResult(RESULT_OK);        
    	finish();
    }
    
    private void populateFields(){
        if (mRowId != null) {
       
        	/**
        	 * This needs to be refactored to the new Book method!!!
        	 */
            Cursor bookmark = mDbHelper.fetchBookmark(mRowId);
            startManagingCursor(bookmark);
            
            int[] lenDistro = mDbHelper.fetchDistro(mRowId);
            
            
            StringBuilder info = new StringBuilder();
            
            info.append("_id: ");
            info.append(bookmark.getString(
                    bookmark.getColumnIndexOrThrow(DbAdapter.KEY_ROWID)));
            info.append("\nTitle: ");
            info.append(bookmark.getString(
                    bookmark.getColumnIndexOrThrow(DbAdapter.KEY_TITLE)));
            info.append("\nSource: ");
            info.append(bookmark.getString(
                bookmark.getColumnIndex(DbAdapter.KEY_SOURCE)));
            info.append("\nLine Position: ");
            info.append(bookmark.getInt(
                    bookmark.getColumnIndex(DbAdapter.KEY_LINE_POSITION)));
            info.append("\nWpm: ");	
            info.append(bookmark.getInt(
                    bookmark.getColumnIndex(DbAdapter.KEY_WPM)));
            info.append("\nCreated: ");
            info.append(bookmark.getInt(
            		bookmark.getColumnIndex(DbAdapter.KEY_CREATED)));
            info.append("\nCompleted: ");
            info.append(bookmark.getInt(
                    bookmark.getColumnIndex(DbAdapter.KEY_COMPLETED)));
            info.append("\nRead Time: ");
            info.append(bookmark.getInt(
                    bookmark.getColumnIndex(DbAdapter.KEY_READ_TIME)));
            info.append("\nWord Count: ");
            
            for( int i = 0; i < lenDistro.length; i++){
            	info.append("\n" + String.valueOf(i)+ "\t" + String.valueOf(lenDistro[i]));
            }
            
            mInfoText.setText(info.toString());
        }
    }
    @Override
    protected void onStop() {
        super.onStop();
        mDbHelper.close();
    }
    
}
