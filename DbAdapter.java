package com.android.flashbang01;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Calendar;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
//import android.widget.Toast;


/**
 * Simple book mark database access helper class. Defines the basic CRUD operations
 * for the book mark example, and gives the ability to list all book mark as well as
 * retrieve or modify a specific book mark .
 * 
 */
public class DbAdapter {

	// Book mark Keys
    public static final String KEY_TITLE = "title";
    public static final String KEY_ROWID = "_id";
    public static final String KEY_SOURCE = "source";
    public static final String KEY_LINE_POSITION = "line_position";    
    public static final String KEY_WORD_POSITION = "word_position";    
    public static final String KEY_WPM = "wpm";
    public static final String KEY_CREATED = "creation_date";
    public static final String KEY_COMPLETED = "completion_date";
    //public static final String KEY_WORD_COUNT = "word_count";  <-- This is included in the length distro now.
    public static final String KEY_READ_TIME = "read_time";
   
    // Length Distro Keys
    public static final String KEY_BOOK_ROWID = "_id";
    public static final String KEY_LENGTH = "length";
    public static final String KEY_COUNT = "count";
   

    private static final String TAG = "BookmarksDbAdapter";
    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDb; 

    /**
     * Database creation sql statement
     */
    private static final String CREATE_TABLE_BOOKMARKS =
    		"create table bookmarks (" +
    				KEY_ROWID + " integer primary key autoincrement, " +
    				KEY_TITLE + " text not null, " +
    		        KEY_SOURCE + " text not null, " +
    				KEY_LINE_POSITION + " integer not null, " +
    				KEY_WORD_POSITION + " integer not null, " +
    		        KEY_WPM + " integer not null, " +
    				KEY_CREATED + " int not null, " +
    				KEY_COMPLETED + " int default null, " +
    				KEY_READ_TIME + " integer not null " +
    				//KEY_WORD_COUNT + " integer" +
    		        ");";
    
    private static final String CREATE_TABLE_LENGTH_DISTRO=
            "create table length_distro (" +
            		KEY_BOOK_ROWID + " integer not null, " +
            		KEY_LENGTH + " integer not null," +
        			KEY_COUNT + " integer not null" +
        	");";

    private static final String DATABASE_NAME = "flashdb";
    private static final String DATABASE_TABLE_BOOKMARKS = "bookmarks";
    private static final String DATABASE_TABLE_LENGTH_DISTRO = "length_distro";
    private static final int DATABASE_VERSION = 10;

    private final Context mCtx;

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
        	db.execSQL(CREATE_TABLE_BOOKMARKS);
        	db.execSQL(CREATE_TABLE_LENGTH_DISTRO);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS bookmarks");
            db.execSQL("DROP TABLE IF EXISTS length_distro");
            onCreate(db);
        }
    }

    public DbAdapter(Context ctx) { 	// Input parameter is the current context
        // This initializes the object with the current context
    	this.mCtx = ctx;
    }

    public DbAdapter open() throws SQLException {
        mDbHelper = new DatabaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        
        return this;
    }

    public void close() {
        mDbHelper.close();
    }



    /**
     * Book Mark Functions
     * 
     */
    
    public Boolean updateBook(Book b){
    	//Log.d("Save charpos",String.valueOf(b.getWordPos()));
    	return updateBookmark(b.getTitle(), b.getSourceFilePath(), b.getRowId(), b.getLinePos(), b.getWordPos(), 
    						b.getWpm(), b.getCreation_date(), b.getCompletion_date(), b.getRead_time());
    }

    public Book createBook(String sourceFilePath, String title){
    	    	
    	// Create the book mark, initialized with charpos = 0.
    	long rowId = createBookmark(title, sourceFilePath, 0, 0);
    	Cursor bookmark = fetchBookmark(rowId);
    	//startManagingCursor(bookmark);
    	
    	int[] lengthDistro;
    	try{
    		BufferedReader sourceFileBuffered = new BufferedReader(new FileReader(sourceFilePath));     
			String line;
			
			lengthDistro = new int[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0/*<-punct*/};
			int punct = 0;
			
			while( (line=sourceFileBuffered.readLine()) != null) {
	            //Find each word in the line and count it's length
				String[] wordArray = line.split(" ");
				
				for(String word : wordArray){
					while(word.endsWith(".") ||
						  word.endsWith(",") ||
						  word.endsWith("?") ||
						  word.endsWith("!") ||
						  word.endsWith(":") ||
						  word.endsWith(";") ){
						punct++;
						word = word.substring(0, word.length()-1);
					}
						
					int len = word.length();
					
					if( len < 20 ){
						lengthDistro[len]++;
						lengthDistro[0]++;
					}else
					if( len >= 20 ){
						lengthDistro[0]++;
						lengthDistro[20]++;
					}
				}
			}
			lengthDistro[21] = punct;
			sourceFileBuffered.close();
    	}
    	catch (IOException e){
    		lengthDistro = new int[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		}
		createDistro(rowId, lengthDistro);

		int wpm = bookmark.getInt(
    			bookmark.getColumnIndexOrThrow(DbAdapter.KEY_WPM));

    	int creationDate = bookmark.getInt(
    			bookmark.getColumnIndexOrThrow(DbAdapter.KEY_CREATED));
    	
    	int compDate = bookmark.getInt(
    			bookmark.getColumnIndexOrThrow(DbAdapter.KEY_COMPLETED));
    	
    	int readTime = bookmark.getInt(
    			bookmark.getColumnIndexOrThrow(DbAdapter.KEY_READ_TIME));

    	bookmark.close();
		return new Book(title, sourceFilePath, rowId, 0,0, wpm, creationDate, compDate, readTime, lengthDistro);
    }
    
    public Book fetchBook(long rowId){
    	
    	try{
	    	//Get the path to the source
	    	Cursor bookmark = fetchBookmark(rowId);
	    	   
	    	String title = bookmark.getString(
	    			bookmark.getColumnIndexOrThrow(DbAdapter.KEY_TITLE));
	
	    	String sourceFilePath = bookmark.getString(
	    			bookmark.getColumnIndexOrThrow(DbAdapter.KEY_SOURCE));
	    	
	    	int linePos = bookmark.getInt(
	    			bookmark.getColumnIndexOrThrow(DbAdapter.KEY_LINE_POSITION));

	    	int wordPos = bookmark.getInt(
	    			bookmark.getColumnIndexOrThrow(DbAdapter.KEY_WORD_POSITION));
	    	
	    	int wpm = bookmark.getInt(
	    			bookmark.getColumnIndexOrThrow(DbAdapter.KEY_WPM));
	    	
	    	int creationDate = bookmark.getInt(
	    			bookmark.getColumnIndexOrThrow(DbAdapter.KEY_CREATED));
	    	
	    	int compDate = bookmark.getInt(
	    			bookmark.getColumnIndexOrThrow(DbAdapter.KEY_COMPLETED));
	    	
	    	int readTime = bookmark.getInt(
	    			bookmark.getColumnIndexOrThrow(DbAdapter.KEY_READ_TIME));
	    	
	    	int[] lenDistro = fetchDistro(rowId);
	    	
	    	bookmark.close();
	        return new Book(title, sourceFilePath, rowId, linePos, wordPos, 
	        		wpm, creationDate, compDate, readTime, lenDistro);
    	}
    	catch(SQLException e){
    		return null;
    	}
    }
    
    public long createBookmark(String title, String source, int linePosition, int wordPosition) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_TITLE, title);
        initialValues.put(KEY_SOURCE, source);
        initialValues.put(KEY_LINE_POSITION, linePosition);
        initialValues.put(KEY_WORD_POSITION, wordPosition);
        
        initialValues.put(KEY_WPM, 200);
        initialValues.put(KEY_CREATED, Calendar.getInstance().getTimeInMillis());
        //initialValues.put(KEY_COMPLETED, 0);
        //initialValues.put(KEY_WORD_COUNT, 0);  
        initialValues.put(KEY_READ_TIME, 0);

        
        return mDb.insert(DATABASE_TABLE_BOOKMARKS, null, initialValues);
    }

    // Delete the book mark with the given rowId
    public boolean deleteBookmark(long rowId) {
        return mDb.delete(DATABASE_TABLE_BOOKMARKS, KEY_ROWID + "=" + rowId, null) > 0;
        //need to also delete the distro stuff.
    }

    // Return a Cursor over the list of all book marks in the database
    public Cursor fetchAllBookmarks() {

        return mDb.query(
        		DATABASE_TABLE_BOOKMARKS, // Database Table name
        		new String[] { //Columns
        				KEY_ROWID, 
        				KEY_TITLE, 
        				KEY_SOURCE, 
        				KEY_LINE_POSITION,
        				KEY_WORD_POSITION,
        				KEY_WPM, 
        		        KEY_CREATED,
        		        KEY_COMPLETED, 
        		        //KEY_WORD_COUNT, 
        		        KEY_READ_TIME,
        			},
        			
        		null, //Selection
        		null, // SelectionArgs
        		null, // groupby
        		null, // having
        		null);// orderby
    }

    // Return a Cursor positioned at the book mark that matches the given rowId
    public Cursor fetchBookmark(long rowId) throws SQLException {

        Cursor mCursor =
        		
            mDb.query(
            		true, // Distinct
            		DATABASE_TABLE_BOOKMARKS, //database table to query  
            		new String[] {
            				KEY_ROWID, 
            				KEY_TITLE, 
            				KEY_SOURCE, 
            				KEY_LINE_POSITION,
               				KEY_WORD_POSITION,
               				KEY_WPM, 
            		        KEY_CREATED, 
            		        KEY_COMPLETED, 
            		        KEY_READ_TIME 
            				
            		}, //list of columns returned
            		KEY_ROWID + "=" + rowId, // Selection
            		null, //??
                    null, // selectionArgs
                    null, // groupBy 
                    null, // having 
                    null);// orderBy
        
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;


    }

    /**
     * Update the book mark using the details provided. The book mark to be updated is
     * specified using the rowId, and it is altered to use the ...
     * 
     * @param rowId id of book mark to update
     * @param title value to set book mark title to
     * @return true if the note was successfully updated, false otherwise
     */
    public boolean updateBookmark(String title, String sourceFilePath, long rowId, int linePos, int wordPos,
    		int wpm, int creationDate, int compDate, int readTime){
        ContentValues args = new ContentValues();
        args.put(KEY_TITLE, title);
        args.put(KEY_SOURCE, sourceFilePath);
        args.put(KEY_ROWID, rowId);
        args.put(KEY_LINE_POSITION, linePos);
        args.put(KEY_WORD_POSITION, wordPos);
        args.put(KEY_WPM, wpm);
        args.put(KEY_CREATED, creationDate);
        args.put(KEY_COMPLETED, compDate);
        args.put(KEY_READ_TIME, readTime);
        
        //Log.d("Save charpos db: ",String.valueOf(charPos));
    	
        return mDb.update(DATABASE_TABLE_BOOKMARKS, args, KEY_ROWID + "=" + rowId, null) > 0;
    }

    public boolean updateBookmarkReadTime(long rowId, int newTime){
    	ContentValues args = new ContentValues();
    	args.put(KEY_READ_TIME, newTime);

    	return mDb.update(DATABASE_TABLE_BOOKMARKS, args, KEY_ROWID + "=" + rowId, null) > 0;    	    	
	}
    
    public boolean updateBookmarkWpm(long rowId, int wpm){
        ContentValues args = new ContentValues();
        args.put(KEY_WPM, wpm);

    	return mDb.update(DATABASE_TABLE_BOOKMARKS, args, KEY_ROWID + "=" + rowId, null) > 0;    	
    }
    
    public boolean updateBookmarkComplete(long rowId){
        ContentValues args = new ContentValues();
        args.put(KEY_COMPLETED, Calendar.getInstance().getTimeInMillis());

    	return mDb.update(DATABASE_TABLE_BOOKMARKS, args, KEY_ROWID + "=" + rowId, null) > 0;
    }
    
    public boolean updateBookmarkPosition(long rowId, int linePosition, int wordPosition) {
        ContentValues args = new ContentValues();
        args.put(KEY_LINE_POSITION, linePosition);
        args.put(KEY_WORD_POSITION, wordPosition);
        
        return mDb.update(DATABASE_TABLE_BOOKMARKS, args, KEY_ROWID + "=" + rowId, null) > 0;
    }

    
    /**
     * Length Distribution Functions
     * 
     */


    public Boolean createDistro(long rowId, int[] counts ) {
    	Boolean retVal = true;
    	
    	for(int i = 0; i < counts.length; i++){
            ContentValues initialValues = new ContentValues();
            initialValues.put(KEY_BOOK_ROWID, rowId);
            initialValues.put(KEY_LENGTH, i);
            initialValues.put(KEY_COUNT, counts[i]);
    		
            if(mDb.insert(DATABASE_TABLE_LENGTH_DISTRO, null, initialValues) == -1)
            	retVal = false;
    	}
        
        return retVal;
    }

    

    public boolean deleteDistro(long rowId) {
        return mDb.delete(DATABASE_TABLE_LENGTH_DISTRO, KEY_BOOK_ROWID + "=" + rowId, null) > 0;
    }

    /*
     * Return a Cursor over the list of all timings in the database
     * 
     * @return Cursor over all timings
     */

    public int[] fetchDistro(long rowId) {

    	int[] wordCount = new int[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0};
    
        Cursor c = mDb.query(
        		DATABASE_TABLE_LENGTH_DISTRO, // Database Table name
        		new String[] {KEY_LENGTH, KEY_COUNT}, //Columns
        		KEY_BOOK_ROWID + " = ?", //Selection
        		new String[] {String.valueOf(rowId)}, // SelectionArgs
        		null, // groupby
        		null, // having
        		null);// orderby
    
        c.moveToFirst();
        while (c.isAfterLast() == false) 
        {
            wordCount[c.getInt(0)]  = c.getInt(1);
            c.moveToNext();
        }
    
        return wordCount;
    }



}
