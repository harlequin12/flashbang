
package com.android.flashbang01;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;



public class Book {

	//Passed from database
	//charPos = bookmark.getInt(
	private long mRowId;
	private String mTitle; //Necessary?
	private String mSourceFilePath;
	
	
	// Settings, state of book
	private int mWpm;
    private int mCreation_date;		//private Date creation_date;
    private int mCompletion_date;   //private Date completion_date;
    private int mRead_time;
    private int[] mLengthDistro;
	
    // Position Management
    // The user's position in the book is marked by a 2d coordinate. 
    private int mLinePos;	// Line number in the book
	private int mWordPos;	// Word number in the current line.
    
	// Not from db
	// Text array
	private List<String> mLineList;
	
	

	public Book(String title, String sourceFilePath, long rowId, int linePos, int wordPos, int wpm, 
				int creationDate, int compDate, int readTime, int[] lengthDistro){
		
		mTitle = title;
		mSourceFilePath = sourceFilePath;
		mRowId = rowId;
		mWpm = wpm;
	    mCreation_date = creationDate;
	    mCompletion_date = compDate;
	    mRead_time = readTime;
	    mLengthDistro = lengthDistro;
	    mWordPos = wordPos;
	    mLinePos = linePos;
	    
	    mLineList = new ArrayList<String>();
		try{
			BufferedReader textFile = new BufferedReader(new FileReader(mSourceFilePath));
			
			String line;
			while((line = textFile.readLine()) != null){
				//if(!line.contentEquals(""))		// Do we want to ignore empty lines in the flash window?
				mLineList.add(line);
			}
			textFile.close();	
		}
		catch (IOException e){
			mLineList.add("Failed to openFile...\n");			
		}
	}
	
	public void decWord(){
		
		mWordPos--;
		if(mWordPos < 0){
			mLinePos--;
		
			if(mLinePos < 0){
				mLinePos = 0;
				mWordPos = 0;
			}else
				mWordPos = mLineList.get(mLinePos).split("[ \n\t\r]").length - 1;
		}	
	}

	public String getWord(){
		return mLineList.get(mLinePos).split("[ \n\t\r]")[mWordPos];	
	}
	
	public void incWord(){		
		mWordPos++;
		
		if(mWordPos >= mLineList.get(mLinePos).split("[ \n\t\r]").length){
			mWordPos = 0;
			mLinePos++;
			if(mLinePos >= mLineList.size())
				mLinePos = mLineList.size()-1;
		}	
	}
	
	/**
	 * Getters
	 */
		
	public List<String> getLineList()	{	return mLineList;		}
	public String getTitle()			{	return mTitle;			}
	public String getSourceFilePath()	{	return mSourceFilePath;	}
	public long getRowId()				{	return mRowId;			}
	public int getWordPos()				{	return mWordPos;		}
	public int getLinePos()				{	return mLinePos;		}
	public int getWpm()					{	return mWpm;			}
	public int getCreation_date()		{	return mCreation_date;	}
	public int getCompletion_date()		{	return mCompletion_date;}
	public int getRead_time()			{	return mRead_time;		}
	public int[] getLengthDistro()		{	return mLengthDistro;	}
	
	
	/**
	 * Setters
	 */
	
	public void setTitle(String title)				{	mTitle = title;					}
	public void setSourceFilePath(String path)		{	mSourceFilePath = path;			}
	public void setRowId(int rowId)					{	mRowId = rowId;					}
	public void setWordPos(int charPos)				{	mWordPos = charPos;				}
	public void setLinePos(int linePos)				{	mLinePos = linePos;				}
	public void setWpm(int wpm)						{	mWpm = wpm;						}
	public void setCreation_date(int crea_date)		{	mCreation_date = crea_date;		}
	public void setCompletion_date(int comp_date)	{	mCompletion_date = comp_date;	}
	public void setRead_time(int read_time)			{	mRead_time = read_time;			}
	public void setLengthDistro(int[] lenDistro)	{	mLengthDistro = lenDistro;		}
	

}
