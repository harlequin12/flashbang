
package com.android.flashbang01;

//import java.io.File;

import java.util.concurrent.ExecutionException;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;
//import com.android.flashbang01.R.array;
//import com.android.flashbang01.R.id;
//import com.android.flashbang01.R.layout;
//import com.android.flashbang01.R.menu;
//import com.android.flashbang01.R.string;
//import android.graphics.Bitmap;
//import android.os.AsyncTask;

/**
 * This is the class that acts on behalf of the main activity.
 * This activity handles listing the book marks and launching them.
 *
 */


public class BookmarkListActivity extends ListActivity {
	
    private static final int ACTIVITY_CREATE=0;
    private static final int ACTIVITY_EDIT=1;
//    private static final int ACTIVITY_READ=2;
    private static final int ACTIVITY_INFO=2;

    private static final int DELETE_ID = Menu.FIRST + 1;
    private static final int EDIT_ID = Menu.FIRST + 2;
    private static final int INFO_ID = Menu.FIRST + 3;

    private DbAdapter mDbHelper;
    private ProgressDialog pd;
    private Context context;
        
   
    /** 
     * Called when the activity (main page - the list of book marks) is first created. 
     * */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.bookmark_list);
        mDbHelper = new DbAdapter(this);
        mDbHelper.open();
        
        context = this;
        
        setTitle("Book Marks");
        
        fillData();
        
        
        registerForContextMenu(getListView());	
    }

    // This function is used to update the list with all the book mark elements
    private void fillData() {
        // Get all of the rows from the database and create the item list
        Cursor bookMarkCursor = mDbHelper.fetchAllBookmarks();
        startManagingCursor(bookMarkCursor);
 
        // Now create a simple cursor adapter and set it to display
        SimpleCursorAdapter notes = new SimpleCursorAdapter(
        		this, 
        		R.layout.bookmark_row, 
        		bookMarkCursor, 
        		// Create array to specify the fields to display in the list 
        		new String[]{DbAdapter.KEY_TITLE,
        					 DbAdapter.KEY_SOURCE},
                // Array of the fields we want to bind those fields to
        	    new int[]{R.id.bmrow_title, R.id.bmrow_source});
        
        setListAdapter(notes);
    }

    //This is called on creation to set up the action menu for the activity
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_menu, menu);
        return true;
    }
            
    public void newBookmark(MenuItem item){
    	// Launch the source dialog
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setTitle(R.string.selectsource);

    	builder.setItems(R.array.source_array, new DialogInterface.OnClickListener() {
    		public void onClick(DialogInterface dialog, int item) {
    			//Launch the appropriate source selection activity
    			switch(item) {
    			case 0:		//File
    				Intent i = new Intent(getBaseContext(), FileListActivity.class);
    				i.putExtra("initDir", Environment.getExternalStorageDirectory().getAbsolutePath());
    				startActivityForResult(i, ACTIVITY_CREATE);
    				break;
    				    
    			case 1:		//Webs
    				break;
    			}		    	
    		}	
    	});
    			
    	AlertDialog alert = builder.create();			
    	alert.show();
    			
    }
    
    public void showPreferences(MenuItem item){
    	Intent i = new Intent(this, DefaultPreferences.class);
    	startActivity(i);
    }         	
    

    // This is called on creation of the activity to initialize the context menu.
    // On my tool this is what happens when you press and hold to select a 
    // menu item.
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, 
    		ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, DELETE_ID, 0, R.string.menu_delete);
        menu.add(0, INFO_ID, 0, "info");
        menu.add(0, EDIT_ID, 0, R.string.menu_edit);
    }

    // This is called when an item in the list is selected by pressing and holding.
    @Override
    public boolean onContextItemSelected(MenuItem item) {
    	// This gets the info on the menu item selected.
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
        
    	switch(item.getItemId()) {
            case DELETE_ID:
                mDbHelper.deleteBookmark(info.id);
                
                // Everything about the book mark needs to be deleted. 
                // Including the distribution stuff
                
                // So I think I need to use the following call instead of the above:
                // mDbHelper.deleteBook(...);
                fillData();
                return true;
            case EDIT_ID:
                Intent i = new Intent(this, BookmarkEditActivity.class);
            	//Toast.makeText(this, "Row id: " + info.id , Toast.LENGTH_SHORT).show();
                i.putExtra(DbAdapter.KEY_ROWID, info.id);
                startActivityForResult(i, ACTIVITY_EDIT);
                return true;
            case INFO_ID:
                Intent n = new Intent(this, BookmarkInfoActivity.class);
            	Toast.makeText(this, "Row id: " + info.id , Toast.LENGTH_SHORT).show();
                n.putExtra(DbAdapter.KEY_ROWID, info.id);
                startActivityForResult(n, ACTIVITY_INFO);
                return true;
            //case ARCHIVE_ID:
        }
        return super.onContextItemSelected(item);
    }


    //Here is where we open a book mark
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
          
        // Launch the flash reader!
        Intent i = new Intent(this, FlashActivity.class);
        //Pass the row id of the book mark
        i.putExtra(DbAdapter.KEY_ROWID, id);
        startActivity(i);        
    }
    
    private class AsyncCreateBook extends AsyncTask<String, Integer, Book> {
		
		protected void onPreExecute() {
			pd = new ProgressDialog(context);
			pd.setTitle("Processing...");
			pd.setMessage("Analyzing Text.");
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();
		}
			
		protected Book doInBackground(String... args) {
			Book b = mDbHelper.createBook(args[0], args[1]);
			return b;
		}
		
		protected void onPostExecute(Book book) {
			if (pd!=null) {
				pd.dismiss();
			}
			fillData();
		}
			
	};
	
 

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        
        if(resultCode == RESULT_OK)
        switch(requestCode){
        case ACTIVITY_CREATE:	
        	// Retrieve the file's path from the intent left behind from the file selector.
        	String sourceFilePath = intent.getStringExtra(DbAdapter.KEY_SOURCE);
        	String bookmarkTitle = intent.getStringExtra(DbAdapter.KEY_TITLE);
        	
        	// We need to check the file to make sure it is usable 
        	// before we attemt to create the bookmark.
        	        	
        	AsyncCreateBook bookTask = new AsyncCreateBook();
          	bookTask.execute(sourceFilePath, bookmarkTitle);       
            	
        	break;
        //case ACTIVITY_EDIT:
        //	break;
       }
       fillData();
    }
    

}
