
package com.android.flashbang01;

import com.android.flashbang01.R;
import com.android.flashbang01.R.id;
import com.android.flashbang01.R.layout;
import com.android.flashbang01.R.string;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class BookmarkEditActivity extends Activity {

	private DbAdapter mDbHelper;

    private EditText mTitleText;
    private EditText mSourceText;
    private Book mBook;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        mDbHelper = new DbAdapter(this);
        mDbHelper.open();
        
        setContentView(R.layout.bookmark_edit);        	
        setTitle(R.string.edit_bookmark);

        mTitleText = (EditText) findViewById(R.id.title);
        mSourceText = (EditText) findViewById(R.id.source);

        Button confirmButton = (Button) findViewById(R.id.confirm);
        
        mBook = mDbHelper.fetchBook(getIntent().getExtras().getLong(DbAdapter.KEY_ROWID));
        if(mBook == null)
        	Toast.makeText(this, "mBook is null", Toast.LENGTH_SHORT).show();
        else
        	Toast.makeText(this, "Editing bm with Row id: " + mBook.getRowId(), Toast.LENGTH_SHORT).show();
        
        
        populateFields();        
    }
    
    public void confirm(View item){
        
        if(mBook != null){
        	String title = mTitleText.getText().toString();
            String source = mSourceText.getText().toString();
            //int charPosition = 0;	//???
            
        	mBook.setTitle(title);
        	mBook.setSourceFilePath(source);
        	mDbHelper.updateBook(mBook);
        }

    	setResult(RESULT_OK);        
    	finish();
    }
    
    private void populateFields(){
    	// This is called when we are editing an already existing bookmark
        if (mBook != null) {
        	
          	mTitleText.setText(mBook.getTitle());
            mSourceText.setText(mBook.getSourceFilePath());     
        }
    }
  
    //If the user presses back before the confirm button is clicked after this activity is brought up
    // to create a new bookmark, then the book mark should not exist in the list. 
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        saveState();
        outState.putSerializable(DbAdapter.KEY_ROWID, mBook.getRowId());
    }
    
    @Override
    protected void onPause() {
        super.onPause();
        saveState();
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        populateFields();
    }
    
    @Override
    protected void onStop() {
        super.onStop();
        mDbHelper.close();
    }
    
    private void saveState() {
        String title = mTitleText.getText().toString();
        String source = mSourceText.getText().toString();
        //int charPosition = 0;	//???
        
        if(mBook != null){
        	mBook.setTitle(title);
        	mBook.setSourceFilePath(source);
        	mDbHelper.updateBook(mBook);
        }
    }
}
